use std::cmp::Eq;
use std::collections::HashMap;
use std::fmt::Debug;
use std::hash::Hash;

use crate::result::*;

mod route;
mod stop;
mod itinerary;
mod itinerary_query;

pub use self::route::Route;
pub use self::stop::Stop;
pub use self::itinerary::{Itinerary, ItineraryElement, BoardVehicleData};
pub use self::itinerary_query::{ItineraryQuery, ItineraryQueryBuilder, ItineraryQueryElement};

pub fn data_lookup<'a, K, V>(map: &'a HashMap<K, V>, key: &K) -> ThprResult<&'a V>
where K: Debug+Eq+Hash {
    map.get(key).ok_or(ThprError::Generic(
        format!("Could not perform data lookup, key {:?} not found", key)).into())
}
