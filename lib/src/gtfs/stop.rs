use std::path::Path;
use chrono_tz::Tz;

use crate::{ThprResult, ThprError};
use super::new_reader_builder;
use super::serde::deser_gtfs_opt_timezone;

#[derive(Deserialize)]
pub struct Stop {
    pub stop_id: String,
    pub stop_code: Option<String>,
    pub stop_name: String,
    pub stop_desc: Option<String>,
    // this could be parsed into a better struct, but for now we won't
    // implement any geo features so we don't care
    pub stop_lat: String,
    pub stop_lon: String,
    pub zone_id: Option<String>,
    pub stop_url: Option<String>,
    pub location_type: Option<i32>,
    pub parent_station: Option<String>,
    #[serde(deserialize_with="deser_gtfs_opt_timezone")]
    pub stop_timezone: Option<Tz>,
    pub wheelchair_boarding: Option<i32>,
}

impl Stop {
    pub fn from_csv(path: &Path) -> ThprResult<Vec<Stop>> {
        let mut reader = new_reader_builder().from_path(path).
            map_err(|e| ThprError::Generic(
                format!("Can't open GTFS file {}: {}", path.to_string_lossy(), e)))?;
        let agencies: Result<Vec<Stop>, _> = reader.deserialize().collect();
        Ok(agencies?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_csv() {
        let stops = &Stop::from_csv(Path::new("../test-data/gtfs/CZ1_1/stops.txt"))
            .expect("Failed parsing stops.txt");
        assert_eq!(stops[0].stop_id, "1");
        assert_eq!(stops[0].stop_name, "Sometown, First");
        assert_eq!(stops[22].stop_id, "23");
        assert_eq!(stops[22].stop_name, "Sometown, Twenty-third");
    }
}
