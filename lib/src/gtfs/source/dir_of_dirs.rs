use std::path::Path;
use std::fs;

use crate::gtfs::Gtfs;
use crate::{ThprResult,ThprError};

pub fn load_all(dir: &Path) -> ThprResult<Vec<Gtfs>> {
    let mut gtfss: Vec<Gtfs> = vec![];
    let dir_entries = fs::read_dir(dir).map_err(|e| ThprError::Generic(
        format!("Can't read GTFS root directory {}: {}", dir.to_string_lossy(), e)))?;
    for entry_res in dir_entries {
        let entry = entry_res?;
        if entry.file_type()?.is_dir() {
            gtfss.push(Gtfs::from_dir(&entry.path())?);
        }
    }
    Ok(gtfss)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_load_all() {
        let gtfss = load_all(Path::new("../test-data/gtfs"))
            .expect("Failed to load dir_of_dirs GTFS source");
        assert_eq!(gtfss.len(), 1);
    }
}
