use chrono::Duration;
use std::path::Path;

use crate::{ThprResult, ThprError};
use super::new_reader_builder;
use super::serde::deser_gtfs_time;

#[derive(Deserialize)]
pub struct StopTime {
    pub trip_id: String,
   // Must use Duration for intraday time, because GTFS allows values
   // above 24:00:00, which Chrono's NaiveTime doesn't allow
    #[serde(deserialize_with="deser_gtfs_time")]
    pub arrival_time: Duration,
    #[serde(deserialize_with="deser_gtfs_time")]
    pub departure_time: Duration,
    pub stop_id: String,
    pub stop_sequence: i32,
    pub stop_headsign: Option<String>,
    pub pickup_type: Option<i32>,
    pub drop_off_type: Option<i32>,
    pub shape_dist_traveled: Option<f32>,
    pub timepoint: Option<i32>,
}

impl StopTime {
    pub fn from_csv(path: &Path) -> ThprResult<Vec<StopTime>> {
        let mut reader = new_reader_builder().from_path(path).
            map_err(|e| ThprError::Generic(
                format!("Can't open GTFS file {}: {}", path.to_string_lossy(), e)))?;
        let agencies: Result<Vec<StopTime>, _> = reader.deserialize().collect();
        Ok(agencies?)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_csv() {
        let times = &StopTime::from_csv(Path::new("../test-data/gtfs/CZ1_1/stop_times.txt"))
            .expect("Failed parsing stop_times.txt");
        assert_eq!(times[0].trip_id, "1");
        assert_eq!(times[0].arrival_time, Duration::hours(8) + Duration::minutes(5));
        assert_eq!(times[0].departure_time, Duration::hours(8) + Duration::minutes(5));
        assert_eq!(times[0].stop_id, "1");
        assert_eq!(times[0].stop_sequence, 1);
        assert_eq!(times[2].trip_id, "1");
        assert_eq!(times[2].arrival_time, Duration::hours(8) + Duration::minutes(8));
        assert_eq!(times[2].departure_time, Duration::hours(8) + Duration::minutes(8));
        assert_eq!(times[2].stop_id, "3");
        assert_eq!(times[2].stop_sequence, 3);
    }
}
