use rusqlite::Connection;
use std::collections::HashMap;

use crate::db::conv::sql_str_array;
use crate::model::Route;
use crate::result::*;

pub fn load_routes(conn: &Connection, route_ids: &[&str]) -> ThprResult<HashMap<String, Route>> {
    let mut stmt = conn.prepare(&format!(
        "SELECT route_id, route_short_name
        FROM routes
        WHERE route_id IN {}",
        sql_str_array(route_ids)?))?;
    let mut rows = stmt.query(rusqlite::NO_PARAMS)?;
    let mut routes = HashMap::new();
    while let Some(row) = rows.next()? {
        let route = Route::from_pos_row(&row)?;
        routes.insert(route.id.clone(), route);
    }
    Ok(routes)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::test::db_w_data;

    #[test]
    fn test_load_routes() {
        let conn = db_w_data();
        let routes = load_routes(&conn, &["1"]).expect("Failed loading routes");
        assert_eq!(routes.len(), 1);
        assert_eq!(&routes.get("1").expect("Route not found").short_name, "30");
    }
}
