SELECT routes.route_id, trips.trip_id, :from_stop_id, :to_stop_id,
    stop_times.departure_time, dest_stop_times.arrival_time
FROM stop_times
    INNER JOIN trips ON stop_times.trip_id = trips.trip_id
    INNER JOIN routes ON trips.route_id = routes.route_id
    INNER JOIN stop_times AS dest_stop_times ON stop_times.trip_id = dest_stop_times.trip_id
    WHERE
        -- match for correct from/to stops
        stop_times.stop_id = :from_stop_id
        AND dest_stop_times.stop_id = :to_stop_id
        -- filter for trips which connect the two stops
        AND stop_times.trip_id IN
            (SELECT tmp_dest_stop_times.trip_id FROM stop_times AS tmp_dest_stop_times
                WHERE tmp_dest_stop_times.stop_id = :to_stop_id)
        -- filter for trips which connect the stops in correct direction
        AND stop_times.stop_sequence <
            (SELECT tmp_dest_stop_times.stop_sequence FROM stop_times AS tmp_dest_stop_times
                WHERE tmp_dest_stop_times.stop_id = :to_stop_id
                    AND tmp_dest_stop_times.trip_id = stop_times.trip_id)
        -- departure time
        AND stop_times.departure_time >= :departure_time
        -- make sure the trip's service schedule says the trip is happening on target date
        AND 1 = (SELECT 1 FROM calendars
            WHERE calendars.service_id = trips.service_id
                -- general fit in the calendar
                AND :departure_date BETWEEN calendars.start_date AND calendars.end_date
                AND (
                    -- days of week match today
                    (calendars.monday = 1 OR :monday = 0)
                    AND (calendars.tuesday = 1 OR :tuesday = 0)
                    AND (calendars.wednesday = 1 OR :wednesday = 0)
                    AND (calendars.thursday = 1 OR :thursday = 0)
                    AND (calendars.friday = 1 OR :friday = 0)
                    AND (calendars.saturday = 1 OR :saturday = 0)
                    AND (calendars.sunday = 1 OR :sunday = 0)
                    -- and there is no "doesn't go" exception
                    AND 2 NOT IN
                        (SELECT exception_type FROM calendar_dates
                            WHERE calendar_dates.service_id = calendars.service_id
                                AND calendar_dates.date = :departure_date)
                    -- or there is "goes" exception
                    OR 1 IN
                        (SELECT exception_type FROM calendar_dates
                            WHERE calendar_dates.service_id = calendars.service_id
                                AND calendar_dates.date = :departure_date)
                )
        )
    ORDER BY dest_stop_times.arrival_time ASC
    LIMIT 1;
