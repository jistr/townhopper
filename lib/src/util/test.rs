use rusqlite::Connection;
use std::path::Path;

use crate::db;
use crate::gtfs::Gtfs;

pub fn db_empty() -> Connection {
    db::open_memory().expect("Failed to create in-memory DB")
}

pub fn db_w_schema() -> Connection {
    let conn = db_empty();
    db::create_schema(&conn).expect("Failed to create DB schema");
    conn
}

pub fn db_w_data() -> Connection {
    let conn = db_w_schema();
    let gtfs = Gtfs::from_dir(Path::new("../test-data/gtfs/CZ1_1"))
        .expect("Failed to load GTFS from directory");

    db::import::import_gtfs(&conn, &gtfs).expect("Failed to import GTFS");
    conn
}
