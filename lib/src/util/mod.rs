use regex::Regex;
use unidecode::unidecode;

pub mod fmt;
pub mod test;

pub fn slug(phrase: &str) -> String {
    let decoded_lower = unidecode(phrase).to_lowercase();
    let replaced = Regex::new("[^a-z]").expect("Failed to create regex")
        .replace_all(&decoded_lower, "-");
    let deduped = Regex::new("-+").expect("Failed to create regex")
        .replace_all(&replaced, "-");
    let trimmed = Regex::new("^-|-$").expect("Failed to create regex")
        .replace_all(&deduped, "");
    trimmed.into()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_slug() {
        assert_eq!("test-of-slug-behavior", slug(":. tESt   of SluG +: běhávíöř - _"));
        assert_eq!("", slug("  "));
    }
}
