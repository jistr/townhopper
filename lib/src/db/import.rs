use rusqlite::{Connection, types::ToSql};
use std::collections::HashSet;

use crate::ThprResult;
use crate::db::{conv, begin, end};
use crate::gtfs::{Agency, Calendar, CalendarDate, Gtfs, Route, Stop, StopTime, Trip};
use crate::util::slug;

pub fn import_gtfs(conn: &Connection, gtfs: &Gtfs) -> ThprResult<()> {
    begin(conn)?;
    validate_agency_tzs(conn, &gtfs.agencies)?;
    import_agencies(conn, &gtfs.agencies)?;
    import_stops(conn, &gtfs.stops)?;
    import_routes(conn, &gtfs.routes)?;
    import_calendars(conn, &gtfs.calendars)?;
    import_calendar_dates(conn, &gtfs.calendar_dates)?;
    import_trips(conn, &gtfs.trips)?;
    import_stop_times(conn, &gtfs.stop_times)?;
    end(conn)?;
    Ok(())
}

fn validate_agency_tzs(conn: &Connection, agencies: &[Agency]) -> ThprResult<()> {
    let mut stmt = conn.prepare("SELECT DISTINCT agency_timezone FROM agencies")?;
    let mut rows = stmt.query(&[] as &[&dyn ToSql])?;
    let mut all_tzs: HashSet<String> = HashSet::new();
    while let Some(row) = rows.next()? {
        all_tzs.insert(row.get::<usize, String>(0)?);
    }
    for a in agencies {
        all_tzs.insert(a.agency_timezone.name().to_string());
    }
    if all_tzs.len() > 1 {
        return Err(format!(
            "Townhopper is currently limited to a single timezone for all agencies \
             imported into a common database. Multiple timezones were found in data: {:?}",
            all_tzs).into())
    }
    Ok(())
}

fn import_agencies(conn: &Connection, agencies: &[Agency]) -> ThprResult<()> {
    let mut stmt = conn.prepare("INSERT INTO agencies (agency_id, agency_name, agency_timezone)
        VALUES (?, ?, ?)")?;
    for a in agencies {
        stmt.execute(&[&a.agency_id as &dyn ToSql, &a.agency_name, &a.agency_timezone.name()])?;
    }
    Ok(())
}

fn import_calendars(conn: &Connection, calendars: &[Calendar]) -> ThprResult<()> {
    let mut stmt = conn.prepare("INSERT INTO calendars
        (service_id, monday, tuesday, wednesday, thursday, friday, saturday, sunday,
            start_date, end_date)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")?;
    for c in calendars {
        stmt.execute(&[&c.service_id as &dyn ToSql,
                       &c.monday, &c.tuesday, &c.wednesday, &c.thursday, &c.friday,
                       &c.saturday, &c.sunday,
                       &conv::to_db_date(&c.start_date), &conv::to_db_date(&c.end_date)])?;
    }
    Ok(())
}

fn import_calendar_dates(conn: &Connection, calendar_dates: &[CalendarDate]) -> ThprResult<()> {
    let mut stmt = conn.prepare("INSERT INTO calendar_dates (service_id, date, exception_type)
     VALUES (?, ?, ?)")?;
    for d in calendar_dates {
        stmt.execute(&[&d.service_id as &dyn ToSql, &conv::to_db_date(&d.date), &d.exception_type])?;
    }
    Ok(())
}

fn import_routes(conn: &Connection, routes: &[Route]) -> ThprResult<()> {
    let mut stmt = conn.prepare("INSERT INTO routes
        (route_id, agency_id, route_short_name, route_long_name, route_type)
        VALUES (?, ?, ?, ?, ?)")?;
    for r in routes {
        stmt.execute(&[&r.route_id as &dyn ToSql, &r.agency_id,
                       &r.route_short_name, &r.route_long_name, &r.route_type])?;
    }
    Ok(())
}

fn import_stops(conn: &Connection, stops: &[Stop]) -> ThprResult<()> {
    let mut stmt = conn.prepare("INSERT INTO stops
        (stop_id, stop_name, stop_slug, wheelchair_boarding)
        VALUES (?, ?, ?, ?)")?;
    for s in stops {
        stmt.execute(&[&s.stop_id as &dyn ToSql, &s.stop_name, &slug(&s.stop_name),
                       &s.wheelchair_boarding])?;
    }
    Ok(())
}

fn import_stop_times(conn: &Connection, stop_times: &[StopTime]) -> ThprResult<()> {
    let mut stmt = conn.prepare("INSERT INTO stop_times
        (trip_id, arrival_time, departure_time, stop_id, stop_sequence)
        VALUES (?, ?, ?, ?, ?)")?;
    for t in stop_times {
        stmt.execute(&[&t.trip_id as &dyn ToSql,
                       &conv::to_db_time(&t.arrival_time), &conv::to_db_time(&t.departure_time),
                       &t.stop_id, &t.stop_sequence])?;
    }
    Ok(())
}

fn import_trips(conn: &Connection, trips: &[Trip]) -> ThprResult<()> {
    let mut stmt = conn.prepare("INSERT INTO trips
        (route_id, service_id, trip_id, direction_id, wheelchair_accessible, bikes_allowed)
        VALUES (?, ?, ?, ?, ?, ?)")?;
    for t in trips {
        stmt.execute(&[&t.route_id as &dyn ToSql, &t.service_id, &t.trip_id, &t.direction_id,
                       &t.wheelchair_accessible, &t.bikes_allowed])?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::test::db_w_data;
    use chrono_tz::Europe::Lisbon;

    #[test]
    fn test_import_gtfs() {
        let c = db_w_data();

        assert_record_count(&c, "agencies", 1);
        assert_record_count(&c, "calendars", 1);
        assert_record_count(&c, "calendar_dates", 1);
        assert_record_count(&c, "routes", 1);
        assert_record_count(&c, "stops", 24);
        assert_record_count(&c, "stop_times", 69);
        assert_record_count(&c, "trips", 3);
    }

    #[test]
    fn test_timezone_validation() {
        let c = db_w_data();
        let different_tz_agency = Agency {
            agency_id: "1".into(),
            agency_name: "1".into(),
            agency_url: "www.something.madeup".into(),
            agency_timezone: Lisbon,
            agency_lang: None,
            agency_phone: None,
            agency_fare_url: None,
            agency_email: None,
        };
        assert!(validate_agency_tzs(&c, &[different_tz_agency]).is_err());
    }

    fn assert_record_count(conn: &Connection, table: &str, count: i32) {
        let mut stmt = conn.prepare(&format!("SELECT COUNT(*) FROM {}", table))
            .expect("Failed to prepare statement");
        let actual_count: i32 = stmt.query_map(&[] as &[&dyn ToSql], |row| row.get(0))
            .expect("Failed to query record count")
            .nth(0)
            .expect("Failed to get result of record count query")
            .expect("Failed to get record count as integer");
        assert_eq!(count, actual_count, "Record count mismatch for {}", table);
    }
}
