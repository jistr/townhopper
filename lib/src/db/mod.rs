use std::fs;
use std::path::Path;
use rusqlite::{Connection, types::ToSql};

use crate::{ThprResult, ThprError};

pub mod import;
pub mod conv;

const DB_SCHEMA_SQL: &'static str = include_str!("db-schema.sql");

pub fn open_file(path: &Path) -> ThprResult<Connection> {
    let c = Connection::open(path)?;
    foreign_keys_on(&c)?;
    Ok(c)
}

pub fn recreate_file(path: &Path) -> ThprResult<Connection> {
    let file_meta = fs::metadata(&path);
    if file_meta.is_ok() {
        fs::remove_file(&path)?;
    }
    let c = Connection::open(path)?;
    foreign_keys_on(&c)?;
    Ok(c)
}

pub fn open_memory() -> ThprResult<Connection> {
    let c = Connection::open_in_memory()?;
    foreign_keys_on(&c)?;
    Ok(c)
}

pub fn create_schema(c: &Connection) -> ThprResult<()> {
    begin(c)?;
    c.execute_batch(DB_SCHEMA_SQL).and(Ok(())).map_err::<ThprError, _>(|e| e.into())?;
    end(c)
}

pub fn foreign_keys_on(c: &Connection) -> ThprResult<()> {
    c.execute("PRAGMA foreign_keys = ON;", &[] as &[&dyn ToSql]).map(|_| ()).map_err(|e| e.into())
}

pub fn foreign_keys_off(c: &Connection) -> ThprResult<()> {
    c.execute("PRAGMA foreign_keys = OFF;", &[] as &[&dyn ToSql]).map(|_| ()).map_err(|e| e.into())
}

pub fn begin(c: &Connection) -> ThprResult<()> {
    c.execute("BEGIN;", &[] as &[&dyn ToSql]).map(|_| ()).map_err(|e| e.into())
}

pub fn end(c: &Connection) -> ThprResult<()> {
    c.execute("END;", &[] as &[&dyn ToSql]).map(|_| ()).map_err(|e| e.into())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_schema() {
        let c = open_memory().expect("Could not open in-memory database");
        create_schema(&c).expect("Schema creation failed");
    }
}
