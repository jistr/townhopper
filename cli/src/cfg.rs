use config::{self, Config, ConfigError};
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::sync::RwLock;
use xdg::BaseDirectories;

use townhopper::gtfs::{Gtfs, source};
use crate::{ThprCliResult, ThprCliError};

lazy_static! {
    static ref CFG: RwLock<Config> = RwLock::new(Config::default());
    static ref CFG_INIT_DONE: RwLock<bool> = RwLock::new(false);
}

pub type GtfsSourceCfg = HashMap<String, String>;

pub struct GSDirOfDirs {
    pub path: PathBuf,
}

pub enum GtfsSource {
    DirOfDirs(GSDirOfDirs),
}
impl GtfsSource {
    pub fn load_all(&self) -> ThprCliResult<Vec<Gtfs>> {
        match self {
            GtfsSource::DirOfDirs(dod) => source::dir_of_dirs::load_all(&dod.path)
                .map_err(|e| e.into()),
        }
    }
}

pub fn init() {
    let mut init_done = CFG_INIT_DONE.write()
        .expect("Failed to get write access to cfg::CFG_INIT_DONE");
    if *init_done {
        panic!("Called cfg::init multiple times");
    }

    populate_defaults()
        .expect("Failed to populate config defaults");

    *init_done = true;
}

pub fn find_config() -> Option<PathBuf> {
    xdg_dirs().find_config_file("config.toml")
}

pub fn get_cache_db_path() -> PathBuf {
    xdg_dirs().place_cache_file("cache.sqlite").expect("Failed to prepare cache.sqlite")
}

pub fn get_gtfs_sources() -> Vec<GtfsSource> {
    let c = CFG.read().expect("Failed to get read access to cfg::CFG");
    let cfgs = c.get::<Vec<GtfsSourceCfg>>("gtfs_sources")
        .expect("Failed to get config value gtfs_sources");
    let cfg_path: PathBuf = c.get::<String>("config_file_path")
        .expect("Failed to get config file path").into();
    let srcs: Result<_, _> = cfgs.into_iter()
        .map(|c| gtfs_source_cfg_to_gtfs_source(&cfg_path, &c))
        .collect();
    srcs.expect("Failed to parse GTFS sources config")
}

pub fn load_config(path: &Path) -> ThprCliResult<()> {
    let mut c = CFG.write().map_err(|e| ThprCliError::from(format!("{}", e).as_str()))?;
    c.merge(config::File::with_name(&path.to_string_lossy()))?;
    c.set("config_file_path", path.parent().unwrap_or(&Path::new(".")).to_str())?;
    Ok(())
}

pub fn validate() -> ThprCliResult<()> {
    if get_gtfs_sources().len() == 0 {
        return Err("No GTFS sources configured.".into());
    }
    Ok(())
}

fn gtfs_source_cfg_to_gtfs_source(cfg_path: &Path, c: &GtfsSourceCfg)
                                  -> ThprCliResult<GtfsSource> {
    let kind_res = c.get("kind").ok_or(ThprCliError::from(
        "Missing 'kind' for gtfs_sources entry."));
    match kind_res?.as_str() {
        "dir_of_dirs" => {
            Ok(GtfsSource::DirOfDirs(GSDirOfDirs {
                path: cfg_rel_path_to_abs(
                    cfg_path,
                    Path::new(c.get("path").ok_or(ThprCliError::from(
                        "Missing 'path' for gtfs_sources entry of dir_of_dirs kind."))?)
                ),
            }))
        },
        s => Err(format!("Unrecognized gtfs_sources kind: {}", s).as_str().into()),
    }
}

fn xdg_dirs() -> BaseDirectories {
    BaseDirectories::with_prefix("townhopper").expect("Failed to create XDG directory info")
}

fn populate_defaults() -> Result<(), ConfigError> {
    let mut c = CFG.write().expect("Failed to get write access to cfg::CFG");
    let xd = xdg_dirs();

    c.set_default::<String>("general.cache_db_path",
                    xd.get_data_home().join("cache.sqlite").to_string_lossy().into())?;
    c.set_default::<Vec<GtfsSourceCfg>>("gtfs_sources", Vec::new().into())?;

    Ok(())
}

fn cfg_rel_path_to_abs(cfg_path: &Path, cfg_rel_path: &Path) -> PathBuf {
    // cfg_rel_path is either already absolute, or it is relative to cfg_path
    if cfg_rel_path.is_absolute() {
        cfg_rel_path.into()
    } else {
        let mut abs_path = PathBuf::new();
        abs_path.push(cfg_path);
        abs_path.push(cfg_rel_path);
        abs_path.canonicalize().expect(
            &format!("Unable to resolve relative path {:?} from config path {:?}.",
                     cfg_rel_path, cfg_path)).into()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_gtfs_source_dir_of_dirs_missing_path() {
        let mut c = GtfsSourceCfg::new();
        c.insert("kind".into(), "dir_of_dirs".into());
        assert!(gtfs_source_cfg_to_gtfs_source(&Path::new("test-townhopper-config"), &c).is_err());
    }

    #[test]
    fn test_gtfs_source_dir_of_dirs() {
        let mut c = GtfsSourceCfg::new();
        c.insert("kind".into(), "dir_of_dirs".into());
        c.insert("path".into(), "../test-data/gtfs".into());
        assert!(gtfs_source_cfg_to_gtfs_source(&Path::new("."), &c).is_ok());
    }
}
