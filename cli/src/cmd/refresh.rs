use clap::{App, SubCommand, ArgMatches};
use townhopper::db;

use crate::ThprCliResult;
use crate::cfg;

pub fn create_subcommand<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("refresh")
        .about("Refresh timetable sources and cache")
}

pub fn run(_top_matches: &ArgMatches, _sub_matches: &ArgMatches) -> ThprCliResult<()> {
    let c = db::recreate_file(&cfg::get_cache_db_path())?;
    db::create_schema(&c)?;
    let sources = cfg::get_gtfs_sources();
    for source in sources.into_iter() {
        for gtfs in source.load_all()?.into_iter() {
            db::import::import_gtfs(&c, &gtfs)?;
        }
    }
    Ok(())
}
