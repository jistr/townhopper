use rusqlite::Connection;
use std::cell::RefCell;
use std::sync::Arc;

use crate::cfg;
use crate::result::*;
use townhopper::db as libdb;

thread_local! {
    static DB: Arc<RefCell<Option<Connection>>> = Arc::new(RefCell::new(None));
}

pub fn connect() -> ThprCliResult<()> {
    DB.with(|arc| {
        arc.replace(Some(libdb::open_file(&cfg::get_cache_db_path())?));
        Ok(())
    })
}

pub fn lease() -> ThprCliResult<DbLease> {
    DB.with(|arc| {
        Ok(DbLease::new(arc.clone()))
    })
}

pub fn begin(c: &Connection) -> ThprCliResult<()> {
    Ok(libdb::begin(c)?)
}

pub fn end(c: &Connection) -> ThprCliResult<()> {
    Ok(libdb::end(c)?)
}

pub struct DbLease {
    arc: Arc<RefCell<Option<Connection>>>,
    conn: Option<Connection>,
}

impl DbLease {
    fn new(arc: Arc<RefCell<Option<Connection>>>) -> Self {
        let conn: Connection = {
            arc.replace(None)
                .expect("Couldn't acquire DB lease -- connection not found")
        };
        DbLease {
            arc: arc,
            conn: Some(conn),
        }
    }

    pub fn as_conn(&self) -> &Connection {
        self.into()
    }
}

impl Drop for DbLease {
    fn drop(&mut self) {
        self.arc.replace(self.conn.take());
    }
}

impl<'a> From<&'a DbLease> for &'a Connection {
    fn from(lease: &'a DbLease) -> &'a Connection {
        lease.conn.as_ref().expect("Lease object contained no DB connection") as &'a Connection
    }
}
