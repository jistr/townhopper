use config;
use std::error::Error;
use std::fmt;

use townhopper::ThprError;

#[derive(Debug)]
pub enum ThprCliError {
    Generic(String),
    Config(config::ConfigError),
    Townhopper(ThprError),
}

impl ThprCliError {
    pub fn generic(msg: &str) -> Box<Self> {
        Box::new(ThprCliError::Generic(msg.into()))
    }
}

impl fmt::Display for ThprCliError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ThprCliError::Generic(ref msg) => write!(f, "ThprCliError::Generic: {}", msg),
            ThprCliError::Config(ref e) => write!(f, "ThprCliError::Config: {}", e),
            ThprCliError::Townhopper(ref e) => write!(f, "ThprCliError::Generic: {}", e),
        }
    }
}
impl Error for ThprCliError {}

impl <'a> From<&'a str> for ThprCliError {
    fn from(message: &'a str) -> Self {
        ThprCliError::Generic(message.to_owned())
    }
}
impl <'a> From<&'a str> for Box<ThprCliError> {
    fn from(message: &'a str) -> Self {
        Box::new(ThprCliError::from(message))
    }
}
impl From<String> for ThprCliError {
    fn from(message: String) -> Self {
        ThprCliError::Generic(message)
    }
}
impl From<String> for Box<ThprCliError> {
    fn from(message: String) -> Self {
        Box::new(ThprCliError::Generic(message))
    }
}

impl From<config::ConfigError> for ThprCliError {
    fn from(error: config::ConfigError) -> Self {
        ThprCliError::Config(error)
    }
}
impl From<config::ConfigError> for Box<ThprCliError> {
    fn from(error: config::ConfigError) -> Self {
        Box::new(ThprCliError::from(error))
    }
}

impl From<ThprError> for ThprCliError {
    fn from(error: ThprError) -> Self {
        ThprCliError::Townhopper(error)
    }
}
impl From<ThprError> for Box<ThprCliError> {
    fn from(error: ThprError) -> Self {
        Box::new(ThprCliError::from(error))
    }
}
impl From<Box<ThprError>> for Box<ThprCliError> {
    fn from(error: Box<ThprError>) -> Self {
        Box::new(ThprCliError::Townhopper(*error))
    }
}

pub type ThprCliResult<T> = Result<T, Box<ThprCliError>>;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_string() {
        let _: ThprCliResult<()> = Err("This is a generic error message".into());
    }

    #[test]
    fn test_thpr_error_is_error() {
        let _: Box<dyn Error> = Box::new(ThprCliError::Generic("Some error".into()));
    }
}
